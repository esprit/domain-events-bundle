<?php

namespace DomainEvents;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Events;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class DomainEventsPublisher implements EventSubscriber
{
    private array $preFlushEvents = [];
    private array $postFlushEvents = [];

    public function __construct(
        private LoggerInterface        $logger,
        private MessageBusInterface    $postFlushBus,
        private ?MessageBusInterface   $preFlushBus = null,
    )
    {
    }

    /**
     * @inheritDoc
     * @uses postFlush
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preFlush,
            Events::postFlush,
        ];
    }

    private function getEventsFromEntity($entity): void
    {
        if (in_array(Traits\DomainEvents::class, class_uses($entity))) {
            while ($e = $entity->getNextEvent()) {
                $this->preFlushEvents[] = $e;
            }
        }
    }

    private function handleEvents(): void
    {
        while ($event = array_pop($this->preFlushEvents)) {
            if (null !== $this->preFlushBus) {
                $this->logger->debug('Handle preFlush event {event} into {bus}', [
                    'event' => get_class($event),
                    'bus' => get_class($this->preFlushBus),
                ]);
                $this->preFlushBus->dispatch($event);
            }

            $this->postFlushEvents[] = $event;
        }
    }

    public function prePersist(LifecycleEventArgs $event)
    {
        $this->getEventsFromEntity($event->getEntity());
        $this->handleEvents();
    }

    public function preFlush(PreFlushEventArgs $event)
    {
        $em = $event->getEntityManager();
        foreach ($em->getUnitOfWork()->getIdentityMap() as $entities) {
            foreach ($entities as $entity) {
                $this->getEventsFromEntity($entity);
            }
        }

        $this->handleEvents();
    }

    public function postFlush()
    {
        while ($event = array_shift($this->postFlushEvents)) {
            $this->logger->debug('Handle preFlush event {event} into {bus}', [
                'event' => get_class($event),
                'bus' => get_class($this->postFlushBus),
            ]);
            $this->preFlushBus->dispatch($event);
        }
    }
}