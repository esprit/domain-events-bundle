<?php

namespace DomainEvents\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('domain_evetns');

        $treeBuilder
            ->getRootNode()
            ->children()
                ->scalarNode('pre_flush_bus')->defaultNull()->end()
                ->scalarNode('post_flush_bus')->defaultValue('messenger.bus.default')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
