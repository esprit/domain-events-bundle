<?php

namespace DomainEvents\DependencyInjection;

use DomainEvents\DomainEventsPublisher;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class DomainEventsExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $preFlushBus = $config['pre_flush_bus'];
        $postFlushBus = $config['post_flush_bus'];

        $definition = new Definition(DomainEventsPublisher::class, [
            new Reference('logger'),
            new Reference($postFlushBus),
            $preFlushBus ? new Reference($preFlushBus) : null,
        ]);
        $definition->addTag('doctrine.event_subscriber');
        $container->setDefinition('domain_event.publisher', $definition);
    }
}
