<?php

namespace DomainEvents\Traits;

trait DomainEvents
{
    private array $_domainEvents = [];

    protected function publishEvent(object $event)
    {
        array_push($this->_domainEvents, $event);
    }

    final public function getNextEvent(): ?object
    {
        return array_shift($this->_domainEvents);
    }
}